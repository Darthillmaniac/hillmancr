function [ans1, ans2] = qe2(a, b, c)
% quadratic solver
ans1 = (2*a)^(-1)*(-b-(((b^2)-(4*a*c))^(0.5)));
product = c/a;
ans2 = product/ans1;
end

