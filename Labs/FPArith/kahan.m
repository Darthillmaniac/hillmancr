function final_sum = kahan(x)
% precision summation
s = x(1);
c = 0;
n = numel(x);
for i = 2:n
    y = x(i) - c;
    t = s + y;
    c = (t - s) - y;
    s = t;
end
final_sum = s;
end

