%% Fan-in function for Lab02
% use |reshape| to create a 2D array with appended 0 as appropriate and
% |sum| to then sum the pairs; repeat ad infinitum until one value remains

function final_sum = fanin(x)
%% fanin: reshape vector x to be 2D and sum the columns
s = x; % s is the vector to be edited
ns = numel(s); % ns is the number of elements of s
while (ns ~= 1) % while it is not a singular value
    if (mod(ns,2) == 1) % if it is an odd number of elements
        s(ns+1) = 0; % append a 0
        ns = numel(s); % revalue ns as appropriate
    end % if 
    s2 = reshape(s,2,ns/2); % s2 is s in columns of 2
    s = sum(s2); % sum pair-wise and save as s
    ns = numel(s); % acquire the number of elements again
end
final_sum = s;
