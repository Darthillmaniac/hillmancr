%% Lab02 - Floating Point Arithmetic
%
% Goal: gain experience with some of the problems associated with floating point
% arithmetic. 
%
%% Instructions
% This file contains a set of example problems and questions, all associated
% with the vagaries of floating point arithmetic. Working exclusively in the 
% branch |FPArith|, complete all the sections in (a copy of) this script. When 
% you are ready to turn-in this lab, make sure you have named the final version
% of the script |FPArith.m|. Then, _tag_ the commit you wish to turn-in as
% follows: 
%
% # Open the *Branches* panel. 
% # Make sure you are in the |FPArith| branch (which is where your work should 
% be).
% # Highlight the particular branch that you'd like tag for submission. (This
% will most likely be the |HEAD| branch.)
% # In _Branch and Tag Creation_ section of the panel, select the radio button
% for a new tag, enter "|Lab02-Submitted|" in the name field, and click on the
% _Create_ button. 
% # Commit and push the this revision to the repository. 
%
% After the due date I will inspect your repository. I will _publish_ the script
% |FPArith| that I find in that commit and grade based on it and the files it
% find in commit tagged |Lab02-Submitted|, and no other. In the event of
% mistakes or other problems I may inspect earlier commits of the |FPArith|
% branch to understand your work up to the final version. _The more complete the
% record of your work in earlier commits, the more likely it will be that I can
% award partial credit._

%% Computing sums
%
% Consider the sum
%
% $$\sum_{k=1}^\infty \frac{1}{k^4}$$
%
% whose value happens to be $\zeta(4)$, where $\zeta$ is the Riemann Zeta
% function. 
%
% * Estimate how many terms are required to compute this sum to double precision
% accuracy. Describe how you came to this estimate. (See the Matlab
% documentation, "Publishing Markup", for help using markup to format your
% explanation.)

%% Recursive summation
%
% Recursive summation computes the sum of a vector of numbers _x(k)_ via the
% strategy
% 
%  Input: a vector x with N elements
%  s := 0; 
%  for k from 1 to N
%    s := s + x(k);
%  end for
%  return s, the sum of the N elements of the vector x
% 
% * Use recursive summation to evaluate $\sum_{k=1}^n k^{-4}$ for $n=200
% 000$. Call the result |gl| (for "|g|reatest to |l|east", the order of the
% terms in the summation). 

%%
% * Matlab knows the $\zeta$ function by the name |zeta|. Compute the difference
% between |gl| and Matlab's |zeta(4)| computation.

%%
% The Matlab function |eps(x)| returns the difference between |x| and the next
% largest floating point value (of the same precision). This difference is
% sometimes referred to as the _unit in the last place_, or _ulp_. A meaningful
% measure of the disparity between different floating point calculations of the
% same quantity is to compare them to the ulp of the result. 
%
% * How many ulp separate |gl| and |zeta(4)|?

%% Recursive summation "greatest to least" vs. "least to greatest"
% Now use recursive summation to compute $\sum_{k=n}^{1} k^{-4}$. Call the
% result |lg| (again, for "|l|east to |g|reatest", the order of the terms in the
% summation).
%
% * How many ulp separate |gl| and |lg|? How many ulp separate |lg| and
% |zeta(4)|? 
%
% Note how in real arithmetic, |gl| and |lg| are the same; in floating point 
% arithmetic they are different. 

%%
% * Which of |gl| and |lg| is closest to |zeta(4)|? Explain why that is to be
% expected.

%% An Alternating Series
% Now consider the summation $s = \sum_{k=1}^\infty (-1)^{k-1}/k^4$. 
%
% * Estimate how many terms are required to compute this sum to double precision
% accuracy. Describe how you came to this estimate.

%%
% Compute $s$ using recursive summation applied to the first $n=200 000$ terms
% in the sum. Carry-out the recursive summation in the order of terms of
% decreasing magnitude (call the result |gl|) and again in the order of
% increasing magnitude (call the result |lg|). 
%
% * Evaluate the difference between |gl| and |lg| in ulps of the result.

%%
% * The sum $s$ happens to be exactly $7\pi^4/720$. Which of |gl| and |lg| is
% closest to correct?

%% The Matlab function |sum|
% The Matlab function |sum(x)| will sum all the elements in the vector |x|.
% 
% * Create a vector |x|, with _200 000_ elements, where $x(k)=k^{-4}$. Use the
% Matlab function |sum| to sum the vector |x| from front to back (call it |xfb|)
% and then again from back to front (call it |xbf|). Compare with |fb| and |bf|,
% which you computed above. What sum does it appear that the Matlab |sum|
% command carries out?  

%%
% There are many ways to compute the sum of a list of numbers. Unless your need
% is to be very accurate, or your sum consists of many terms of very different
% magnitudes and different signs (so that there is a lot of cancellation) whose
% it generally doesn't really matter which way you do computation and it is best
% to use |sum|. 
%
% Nevertheless, two different summation algorithms - _fan-in_ summation and
% _compensated_ summation - deserve special notice.

%% Fan-in Summation
% Fan-in summation, also known as _pairwise_ summation, sums adjacent pairs of
% numbers to form a new vector, repeating the process over and over until there
% all the numbers are summed. In pseudo-code:
%
%  Input: vector x with n elements
%  s := x
%  ns := n
%  while ns > 1 do
%    if ns is odd
%      s := s with 0 appended to the end;
%      ns := number of elements of s
%    end if
%    for k = 1 to (ns/2) do
%      s2(k) := s(2*k-1) + s(2*k)
%    end for
%    s := s2
%    ns := number of elements of s
%  end while
%  return s, the (fan-in) sum of the components of x
%
% * Create a matlab _function_, called |fanin|, that takes a vector |x| and
% returns the sum of its elements, evaluated using using the fan-in algorithm.
% [In writing your function, look-up and use the matlab functions |reshape| and
% |sum| to eliminate the |for/end for| loop, and lookup and use |numel| to get
% the number of elements in a vector.] Test your implementation on several sums
% like the ones above. Show those tests below. Discuss.  

%% Compensated Summation
% Kahan's compensated summation algorithm is appropriate when you have to
% squeeze-out every last bit of precision from your summation. It takes
% advantage of knowing exactly how IEE754 arithmetic works. In pseudo-code, the
% algorithm goes as follows: 
%
%  Input: a vector x with n elements
%  s := x(1)
%  c := 0
%  for i from 2 to n do
%    y := x(i) - c
%    t := s + y
%    c := (t - s) - y % the order is important, and the parentheses too!
%    s := t 
%  end for
%  return s, the sum of the components of x
%
% * Create a matlab _function_, called |kahan|, that takes a vector |x| and
% returns the sum of its elements, evaluated using using Kahan's algorithm. Test
% it on several sums like the ones above. Show those tests below.

%% A Final Note on Summation
% No single algorithm will work every time and in every case. When dealing with
% a sum (any problem, really) it is important to understand the scale of the
% terms you will be working with, the accuracy of the result you need, and
% choose your method of solution appropriately. In the case of summations, _in
% designing or choosing a summation method to achieve high accuracy, the aim
% should be to mininize the absolute values of all intermediate sums* (pg. 82,
% Higham, N. J.: Accuracy and Stability of Numerical Algorithms, Second Edition.
% SIAM. 2002.) 

%% Catastrophic cancellation
% * Evaluate the quantity |y1=(1-cos(x))./x^2 for |x=logspace(-9,-7)|. Use
% |semilogx| to plot |y1| vs. |x|. Is |y1| equal to the floating point value of
% $(1-cos(x))/x^2$ for all $x$?

%% Catastrophic cancellation amplifies errors made earlier in a calculation
%
% Owing both to representation error (|x| $\neq x$) and computation error
% ($\cos\neq$ |cos|), $\cos(x)$ is not equal to |cos(x)|. For small $x$ the
% error is small: only a couple of ulp of |cos(x)|. Nevertheless, that small
% error is made very apparent when the quantity |1-cos(x)| is computed. For
% small |x| the floating point subtraction |1-cos(x)| is exact: i.e., there is
% no computation error. What has happened is that the subtraction has caused the
% errors made earlier, in the floating point calculation of $\cos(x)$ (i.e.,
% |cos(x)|), to be magnified. 
%
% With this understanding we can see a way around the problem of computing
% $1-\cos(x)$. Taking advantage of the trig identity 
%
% $$\cos x = \cos^2 \frac{x}{2} - \sin^2\frac{x}{2} = 1-2\sin^2\frac{x}{2}$$
%
% we see that $1-\cos x = 2\sin^2(x/2)$. While representation and computation
% error will be present when we evaluate |sin(x/2)^2| for $\sin^2(x/2)$, these
% will be small compared to the magnitude of $\sin^2(x/2)$ (no more than a few
% ulp); |2*sin(x/2)^2| is a much more accurate representation of $1-\cos(x)$
% than is |1-cos(x)|. 
%
% * Evaluate $(1-\cos(x))/x^2$ as |y2 = (sin(x/2)/(x/2))^2| for
% |x=logspace(-10,-7)|. Plot both |y1| and |y2| against |x| using |semilogx|.
% Discuss. (*Important note:* to include a graph in a published document, you
% must introduce the |snapnow| command at the appropriate place. Lookup
% |snapnow| in the Matlab documentation for examples.)

%% Catastrophic cancellation and roots of the quadratic equation
% The quadratic equation $ax^2+bx+c=0$ has two solutions: 
%
% $$x = (2a)^{-1}\left(-b\pm\sqrt{b^2-4a}\right).$$
%
% * Write a function that takes as arguments $a$, $b$, and $c$, and returns the
% two roots, computed exactly as written above. Name the function |qe|. Test the
% function for a few different $a$, $b$, and $c$. Show your tests below. 

%%
% * Let |a=c=1| and |b=logspace(6,7.5,1001)|. Use |semilogx| to plot the product
% of the roots (which should be unity) as computed by |qe|. Discuss and explain
% the plot.

%%
% When $|b|\gg4ac$ then one of the roots may be very inaccurate owing to
% catastrophic cancellation. 
%
% * In the previous example, which root is inaccurate? What is the source of the
% error that is magnified by a catastrophic cancellation? Describe how the
% different calculations in the evaluation of the inaccurate root magnify the
% error. 

%%
% To avoid this particular catastrophic cancellation, note that one of the roots
% is accurately calculated, and that the product of the roots is also accurately
% known. Thus, we can evaluate the root that can be accurately calculated and
% divide it into the product of the roots to obtain an accurate calculation of
% the second root. 
%
% * Create the Matlab function |qe2|, which evaluates the roots of the quadratic
% equation by first calculating, in the usual manner, the root that does not
% involve a catastrophic cancellation for $b^2\gg 4ac$, and then calculates the
% second root by dividing the first into the product of the roots. Test your
% function for a few different $a$, $b$, $c$. Show your tests below. 

%%
% * Let |a=c=1| and |b=logspace(7,8,1000)|. Evaluate both roots of the
% quadratic equation, first using |qe| and second using |qe2|. Use |semilogx| to
% plot the two roots against |b|. Discuss and explain the plot.

%% An exercise in avoiding catastrophic cancellation
% * How would you accurately compute $z = (x+y)^{1/4}-y^{1/4}$ for $x>0$, $y>0$?
% Give examples showing how your algorithm for computing $z$ is better than the
% straightforward implementation of the formula given above. 

%% Catastrophic Cancellation and Relativistic Kinetic Energy
% A particle with rest mass $m_0$ is moving with velocity $v$. $v$ is measured
% in units of the speed of light, so that $0\leq |v| < 1$. The particle's
% kinetic energy is its total (relativistic) energy less its rest mass energy:
% i.e., $m_0(\gamma-1)$. 
%
% * How should you go about calculating the quantity $\gamma-1$
% so that it is accurately computed for all $0\leq|v|<1$? Show how your
% algorithm is better than the evaluating $\gamma-1$ as written. 
