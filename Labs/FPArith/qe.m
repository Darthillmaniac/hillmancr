function [ans1, ans2] = qe(a, b, c)
% quadratic solver
ans1 = (2*a)^(-1)*(-b+(((b^2)-(4*a*c))^(0.5)));
ans2 = (2*a)^(-1)*(-b-(((b^2)-(4*a*c))^(0.5)));
end

